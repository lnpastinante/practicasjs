// Prototypal Inheritance
// myPerson --> Person.prototype --> Object.prototype --> null

// ##################################CONSTRUCTOR SHORT SYNTAX########################################

class PersonClass {
    constructor(firstName, lastName, age, likes = []) {
        this.firstName = firstName
        this.lastName = lastName
        this.age = age
        this.likes = likes
    }
    getBio() {
        let bio = `${this.firstName} is ${this.age}.`

        this.likes.forEach((like) => {
            bio = bio + ` ${this.firstName} likes ${like}.` // We are able to access this beacause arrow functions do not override this
        })
    
        return bio
    }
    setName (fullName) {
        const names = fullName.split(' ')
        this.firstName = names[0]
        this.lastName = names[1]
    }
}


const myPerson = new PersonClass('squidward', 'The squid', 30, ['playing music'])
console.log(myPerson)
console.log(myPerson.getBio())

// ##########################CONSTRUCTOR LONG SYNTAX###########################################################

const Person = function (firstName, lastName, age, likes = []) {
    this.firstName = firstName
    this.lastName = lastName
    this.age = age
    this.likes = likes
}

Person.prototype.getBio = function () {
    let bio = `${this.firstName} is ${this.age}.`

    this.likes.forEach((like) => {
        bio = bio + ` ${this.firstName} likes ${like}.` // We are able to access this beacause arrow functions do not override this
    })

    return bio
}

Person.prototype.setName = function(fullName) {
    const names = fullName.split(' ')
    this.firstName = names[0]
    this.lastName = names[1]
}

const me = new Person('Lisandro', 'Pastinante', 27, ['programming', 'learning'])
me.setName('Patrick Star')

console.log(me.getBio())

const bob = new Person('Spongebob', 'Squarepants', 20, ['working', 'drawing'])

console.log(bob.getBio())

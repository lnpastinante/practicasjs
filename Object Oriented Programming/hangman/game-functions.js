'use strict'

//variables
const mainDiv = document.querySelector('#main-div')
const guessInput = document.querySelector('#guess')
const Gletters = document.querySelector('#guessed-letters')
const Wletters = document.querySelector('#wrong-letters')
const Rtrys = document.querySelector('#remaining-trys')


const wrongL = []
const guessedL = []
const filteredLetters = []
let trys
let lost = false

// Generates Prototype Object 
const Hangman = function (letters,guessedLetters=[]) {
    this.letters = letters.toUpperCase().split('')
    this.guessedLetters = guessedLetters
}

// Generates the visible part of the puzzle
Hangman.prototype.getPuzzle = function() {
    mainDiv.textContent= ''
    
    this.letters.forEach((letter) => {
        const letterDiv = createDOM('div','letter',mainDiv)
        if (this.guessedLetters.includes(letter.toUpperCase()) || letter === ' ') {
            letterDiv.textContent = letter.toUpperCase()
            letterDiv.setAttribute('style', 'background-color: rgb(98, 235, 98);')
        } else {
            lost ? letterDiv.textContent = letter.toUpperCase() :letterDiv.textContent = '-'
        }
    })   
    this.check()
    if(!lost){this.checkEnd()} 
}


// Checks for correct and worng words

Hangman.prototype.check = function(){
    this.guessedLetters.forEach((GL)=>{
        if (this.letters.includes(GL)) {
            if(!guessedL.includes(GL)){guessedL.push(GL)} 
        } else {
            if(!wrongL.includes(GL)) wrongL.push(GL)
        }
    })
    
    this.letters.forEach((L)=> {
        if(!guessedL.includes(L) && L !== ' ' && !filteredLetters.includes(L)) {
            filteredLetters.push(L)
        }
    })

    trys = totalTrys - wrongL.length

    Gletters.textContent = guessedL
    Wletters.textContent = wrongL
    Rtrys.textContent = trys
}

// Checks for win/lose condition
Hangman.prototype.checkEnd = function() {
    if (trys === 0){
        lost = true
        this.getPuzzle()
        document.querySelector('#lose').setAttribute('style', 'visibility:visible;')
        setTimeout(()=>{location.assign('./index.html')} , 2000)
    }
    else if (filteredLetters.length === guessedL.length) {
        document.querySelector('#win').setAttribute('style', 'visibility:visible;')
        setTimeout(()=>{location.assign('./index.html')} , 2000)
    }
}

// Create DOMelement Function

const createDOM = (elementTag,elementClass,appendTo)=>{
    const element = document.createElement(elementTag)
    element.setAttribute('class', elementClass)
    appendTo.appendChild(element)
    return element
}
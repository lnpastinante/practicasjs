'use strict'

wordInput.focus()

// Event enter presses play
window.addEventListener('keydown', (e) => {
    if(e.key === 'Enter') {playButton.click()}
})

// Event play button clicked
playButton.addEventListener('click', ()=> {
    if(wordInput) {    
        const word = wordInput.value
        localStorage.setItem('word', word)
        location.assign('./game.html')
    }
})

// Event check for correct input
wordInput.addEventListener('input', (e)=>{
    if (!alphaOnly(e)) {
        e.target.value = ''
        wordInput.blur()
        rule2.setAttribute('style', 'color:red;font-size:2rem;')
        setTimeout(()=>{wordInput.focus()},1000)
    }
})

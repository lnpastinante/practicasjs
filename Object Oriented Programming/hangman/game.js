'use strict'

const word = new Hangman(localStorage.getItem('word'))
const totalTrys = Math.floor((word.letters.length)/2)+1
word.getPuzzle()

guessInput.focus()

// Event Listener: user inputs a letter
guessInput.addEventListener('input', (e) => {
    if (!word.guessedLetters.includes(e.target.value.toUpperCase())){
    word.guessedLetters.push(e.target.value.toUpperCase())
    setTimeout(()=>{e.target.value= ''}, 333)
    word.getPuzzle()
    }
    else{
        document.querySelector('.hint').setAttribute('style', 'visibility:visible;')
        setTimeout(()=>{
            e.target.value= ''
            document.querySelector('.hint').setAttribute('style', 'visibility:hidden;')
        }, 1000)
    }
})
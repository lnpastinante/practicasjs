class Person {
    constructor(firstName, lastName, age, likes) {
        this.firstName = firstName
        this.lastName = lastName
        this.age = age
        this.likes = likes
    }
    getBio() {
        let bio = `${this.firstName} is ${this.age}.`

        this.likes.forEach((like) => {
            bio = bio + ` ${this.firstName} likes ${like}.` // We are able to access this beacause arrow functions do not override this
        })
    
        return bio
    }
    setName (fullName) {
        const names = fullName.split(' ')
        this.firstName = names[0]
        this.lastName = names[1]
    }
}



class Employee extends Person {
    constructor(firstName, lastName, age, position, likes) {
        super(firstName, lastName, age, likes)
        this.position = position
    }
    getBio() {
        let bio = `${this.firstName} is a ${this.position}.`

        this.likes.forEach((like) => {
            bio = bio + ` ${this.firstName} likes ${like}.` // We are able to access this beacause arrow functions do not override this
        })
    
        return bio
    }
    getYearsLeft() {
        return 65 - this.age
    }
}

const myPerson = new Person('Squidward', 'The squid', 30, ['playing music'])
console.log(myPerson)
console.log(myPerson.getBio())

const myEmployee = new Employee('Squidward', 'The squid', 30, 'Cashier', ['playing music'])
console.log(myEmployee)
console.log(myEmployee.getBio())
console.log(myEmployee.getYearsLeft())


// Create a class for students that extends from Person
// tracks student grade 0 - 100
// override bio to print a passing or failing message. 70 or more pass
// Create updateGrade that takes the ammout to add or remove from grade

// Create Student
// Print status (pass or fail)
// Change grade to change status
// Print status again

class Student extends Person {
    constructor(firstName, grade) {
        super(firstName)
        this.grade = grade
    }
    getBio(){
        return this.grade >= 70 ? `${this.firstName} is passing` : `${this.firstName} is failing`
    }
    updateGrade(add) {
        this.grade += add
    }
}

const pepe = new Student('Pepe', 75)

console.log(pepe.getBio())

pepe.updateGrade(-10)

console.log(pepe.getBio())

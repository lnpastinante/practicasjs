// Load data from localStorage

loadTodos = function(toDo) {
    const savedTodos = localStorage.getItem('toDo')
    if (savedTodos !== null){
    return JSON.parse(savedTodos)}
    else{return []}
}

// Save data to localStorage

saveTodos = function(toDo) {
    const dataToStore = JSON.stringify(toDo)
    localStorage.setItem('toDo',dataToStore)
}

// Remove todos
const removeTodo = function(id){
    const removedTodoIndex = toDo.findIndex(function(item){
        return item.id === id
    })
    if (removedTodoIndex > -1) {
        toDo.splice(removedTodoIndex,1)
    }
}

// Things left to do

const thingsLeft = function() {
    unfinished = toDo.filter(function(item) {
    return item.completed === false})
    document.querySelector('#things-left').innerHTML = `You have ${unfinished.length} tasks left`
} 

// Render toDos by text filter

const searchText = function(array, filter) {
    const filteredTodos = array.filter(function(item){
        if (hideCompleted === false) {
            return item.text.toLowerCase().includes(filter.text.toLowerCase())
        }
        else {
            return (item.text.toLowerCase().includes(filter.text.toLowerCase()) && item.completed === false)
        }
    })

    document.querySelector('#todos').innerHTML = ''

    filteredTodos.forEach(function(item){
        const thingTodoDiv = document.createElement('div')
        document.querySelector('#todos').appendChild(thingTodoDiv)

        // Create checkbox
        const ThingTodoCheckbox = document.createElement('input')
        ThingTodoCheckbox.setAttribute('type', 'checkbox')
        thingTodoDiv.appendChild(ThingTodoCheckbox)
        if (item.completed === true) {
            ThingTodoCheckbox.setAttribute('checked', true)
        }
        ThingTodoCheckbox.addEventListener('change', function(e) {
            item.completed = e.target.checked
            thingsLeft()
            saveTodos(toDo)
        })

        // Create todo
        const thingTodo = document.createElement('span')
        thingTodo.textContent = item.text
        thingTodoDiv.appendChild(thingTodo)

        //  Create remove button
        const button = document.createElement('button')
        button.textContent = 'x'
        thingTodoDiv.appendChild(button)
        button.addEventListener('click', function(){
            removeTodo(item.id)
            searchText(toDo,search)
            saveTodos(toDo)
        })
        thingsLeft()
    })
}
let toDo = []  

// {text: '', completed: false}

/* const p = document.querySelectorAll('p')

p.forEach(function(todo){
    if (todo.textContent.includes('to')) {
        todo.remove()
    }
}) */

// You have 2 todos left (p element)
// Add a p for each todo above (use text value)

// Setup a div that contains all todos
// Setup filters (searchText) and wire up a new filter input to change it
// Create a render todos function to render and rerender the latest filtered data

const search = {
    text:''
}

const savedTodos = localStorage.getItem('toDo')
if (savedTodos !== null){
    toDo = JSON.parse(savedTodos)
}

console.log(toDo)

let hideCompleted = false

const searchText = function(array, filter) {
    const filteredTodos = array.filter(function(item){
        if (hideCompleted === false) {
            return item.text.toLocaleLowerCase().includes(search.text.toLocaleLowerCase())
        }
        else {
            return (item.text.toLocaleLowerCase().includes(search.text.toLocaleLowerCase()) && item.completed === false)
        }
    })

    document.querySelector('#todos').innerHTML = ''

    filteredTodos.forEach(function(item){
        const thingTodo = document.createElement('p')
        thingTodo.textContent = item.text
        document.querySelector('#todos').appendChild(thingTodo)
    })
    thingsLeft = toDo.filter(function(item) {
        return item.completed === false
    })
    document.querySelector('#things-left').innerHTML = `You have ${thingsLeft.length} tasks left`
}

searchText(toDo,search)



// Create a form with a single input for todo text
// Setup a submit handler and cancel the default action
// Add a new item on the todos array with entered text data and completed: false
// Rerender app
// clear input field value

/* 
document.querySelector('#add-form').addEventListener('submit', function(e) {
    e.preventDefault()
    newForm = {text:e.target.elements.task.value, completed: false}
    toDo.push(newForm)
    e.target.elements.task.value = ''
    searchText(toDo,search)
})
  */

document.querySelector('#filter-task').addEventListener('input', function(e) {
    search.text = e.target.value
    searchText(toDo,search)
})

// Create a checkbox and setup event listener --> Hide completed
// Create a new filter alongside the old one called hideCompleted (defalt: false)
// Update hideCompleted and render list on checkbox change
// Setup renderTodos to remove completed items

document.querySelector('#hide-completed').addEventListener('change', function(e) {
    hideCompleted = e.target.checked
    searchText(toDo,search)
})

// Delete dummy data
// Read and parse the data when the app starts
// stringify and write data when new data is added

document.querySelector('#add-form').addEventListener('submit', function(e) {
    e.preventDefault()
    newForm = {text:e.target.elements.task.value, completed: false}
    toDo.push(newForm)
    e.target.elements.task.value = ''
    searchText(toDo,search)
    dataToStore = JSON.stringify(toDo)
    localStorage.setItem('toDo',dataToStore)

})
let notes = []

const filters = {
    searchText: ''
}

// Check for existing saved data
const notesJSON = localStorage.getItem('notes')

if(notesJSON !== null) {
    notes = JSON.parse(notesJSON)
}

const renderNotes = function(notes, filters) {
    const filteredNotes = notes.filter(function(note) {
        return note.title.toLowerCase().includes(filters.searchText.toLowerCase())    
    })

    document.querySelector('#notes').innerHTML = ''

    filteredNotes.forEach(function(note) {
        const noteElement = document.createElement('p')
        if(note.title.length > 0) {
            noteElement.textContent = note.title
        }
        else {
            noteElement.textContent = 'Unnamed note'
        }
        document.querySelector('#notes').appendChild(noteElement)
    })
}

renderNotes(notes, filters)

document.querySelector('#search-text').addEventListener('input', function(e){
    filters.searchText = e.target.value
    renderNotes(notes, filters)
})

document.querySelector('#create-note').addEventListener('click', function(e) {
    notes.push({
        title: '',
        body: ''
    })
    localStorage.setItem('notes', JSON.stringify(notes))
    renderNotes(notes, filters)
})

// localStorage.setItem('location', 'Rosario')
// console.log(localStorage.getItem('location'))
// localStorage.removeItem('location')
// localStorage.clear() //deletes everything

/* 
const user = {
    name: 'Andrew',
    age: 27
}
//JavaScript Object Notation
const userJSON = JSON.stringify(user)
console.log(userJSON)
localStorage.setItem('user', userJSON)
  */
/* 
  const userJSON = localStorage.getItem('user')
  const user = JSON.parse(userJSON)
  console.log(`${user.name} is ${user.age}`) 
  */

/* 
document.querySelector('#filter-by').addEventListener('change', function(e){
    console.log(e.target.value)
})
 */

/* 
document.querySelector('#for-fun').addEventListener('change', function(e){
    console.log(e.target.checked)
})
  */


/* 
document.querySelector('#name-form').addEventListener('submit', function (e){
    e.preventDefault()
    console.log(e.target.elements.firstName.value)
    e.target.elements.firstName.value = ''
})
  */


/* 
document.querySelector('#create-note').addEventListener('click', function(e) {
     e.target.textContent = 'The button was clicked' 
})
 */

/* document.querySelector('#remove-all').addEventListener('click', function() {
    document.querySelectorAll('.note').forEach(function(note){
        note.remove()
    })
}) */



/* // DOM - Document Object Model

// const p = document.querySelector('p') //querySelector selects first match

// p.remove()

const ps = document.querySelectorAll('p') //querySelectorAll selects all matches

ps.forEach(function(p) {
    //p.remove()
    //console.log(p.textContent)
    p.textContent = '#########'
})

// Add a new element

const newParagraph = document.createElement('p')
newParagraph.textContent = 'This is a new element from JS'
document.querySelector('body').appendChild(newParagraph) */
'use strict'

const wordInput = document.querySelector('#word-input')
const playButton = document.querySelector('#play-button')
const rule2 = document.querySelector('#rule2')
const rulesDiv = document.querySelector('#rules')

// Returns true for Alphabetical values
const alphaOnly = (e) => {
  const a = e.target.value
  const code = a.charCodeAt(a.length-1)
  return ((code >= 65 && code <= 90) || (code >= 97 && code<=122) || code === 32 || code === 241 || code === 8 || !code)
}
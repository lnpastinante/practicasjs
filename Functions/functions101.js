// function: input(argument), code, output (return value)

let greetUser = function () {
    console.log('Welcome')
}

greetUser()

let square = function (num) {
    let result = num*num
    return result
}

let value = square(3)

console.log(value)
console.log(square(4))
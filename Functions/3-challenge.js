let tipCalc = function(total = 0, percentage = 0.2) {
    if (total<0 || percentage<0) {
        return 'The total and the percentage cannot be negative'
    }
    else {
        return `A ${percentage*100}% tip on ${total}$ would be ${total*percentage}$ ` 
    }
}

let tip = tipCalc(40, 0.25)
console.log(tip)
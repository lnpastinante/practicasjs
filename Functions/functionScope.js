//global scope: convert
// Local scope: F celcius
    //Local scope: isFreezing

let convert = function (F) {
    let celcius = (F - 32)*5/9

    if (celcius <= 0){
        let isFreezing = true
    }

    return celcius
}


console.log("32ºF = "+ convert(32)+"ºC")
console.log("68ºF = "+ convert(68)+"ºC")
//Multiple arguments
let add = function(a, b, c){
    return a + b + c
}

 let result = add(10, 1, 5)
console.log(result)

//Default arguments
let getScoreText = function (name = 'Anon', score = 0) {
    return 'Name: '+ name + ' ~ Score: ' + score
}

let scoreText1 = getScoreText('Pasty',100)
console.log(scoreText1)

let scoreText2 = getScoreText(undefined,50)
console.log(scoreText2)
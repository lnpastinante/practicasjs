let name = 'Slakan'
console.log(`Hey my name is ${name}!`)


//Default arguments
let getScoreText = function (name = 'Anon', score = 0) {
//    return 'Name: '+ name + ' ~ Score: ' + score
      return `Name: ${name} ~ Score: ${score} `
}

let scoreText1 = getScoreText('Pasty',100)
console.log(scoreText1)

let scoreText2 = getScoreText(undefined,50)
console.log(scoreText2)
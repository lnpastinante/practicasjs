// HTTP Hypertext Transfer Protocol
// Request - What do we want to do
// Response - What was actually done


//Making an HTTP request

const request = new XMLHttpRequest()

request.addEventListener('readystatechange', (e)=>{
    if(e.target.readyState === 4) {
        const data = JSON.parse(e.target.responseText)
        console.log(e.target)
        console.log(data)
    }
})

request.open('GET', 'http://puzzle.mead.io/puzzle')
request.send()
// Lexical Scope (Static Scope)
// Global Scope: defined outside code blocks
//Local Scope: defined inside a code block
//Can only access variables defined in a parent or inside the block

let varOne = 'varOne'
if (true){
    console.log(varOne)
    let varTwo = 'varTwo'
}

console.log(varTwo)
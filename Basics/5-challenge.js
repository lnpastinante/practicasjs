let age = 27
let isChild = age <= 7
let isSenior = age >= 65

if (isChild){
    console.log('You are getting the child discount')
}
else{
    if(isSenior){
        console.log('You are getting the senior discount')
    }
    else{
        console.log('You are paying full price')
    }
}
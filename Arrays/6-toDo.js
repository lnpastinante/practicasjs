const toDo = [
    {text: 'Wake up', completed: true},
    {text: 'Have breakfast', completed:false},
    {text: 'Study', completed:true},
    {text: 'Go to work', completed: true},
    {text: 'Do some exercise', completed: false},
]

const deleteTask = function(array, text) {
    index = array.findIndex(function(item){
        return item.text.toLowerCase() === text.toLowerCase()
        
    })
    array.splice(index,1)
}

const getThingsToDo = function(array) {
    return toDo.filter(function(item) {
        return item.completed === false
    }) 
}

console.log(getThingsToDo(toDo))

//deleteTask(toDo, 'study')
//console.log(toDo)
// Create an array with five todos
// You have x  todos
// Print the first and second to last items

const toDo = ['wake up', 'have breakfast', 'study', 'go to work', 'do some exercise']

console.log(`You have ${toDo.length} todos`)
console.log(`Todo: ${toDo[0]}, ${toDo[toDo.length-2]}`)
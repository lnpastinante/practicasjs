
const account = {
    name: 'Lisandro Pastinante',
    income: [],
    expenses: [],
    addExpense: function (description, amount) {
        this.expenses.push({description: description, amount: amount})},
    addIncome: function(description, amount) {
        this.income.push({description: description, amount: amount})
    },
    getAccountSummary: function () {
        let totalIncome = 0
        this.income.forEach(function(item){
            totalIncome = totalIncome + item.amount
        })
        let totalExpenses = 0
        this.expenses.forEach(function(item){
            totalExpenses = totalExpenses + item.amount
        })
        let balance = totalIncome - totalExpenses
        return `${this.name} has a balance of $${balance}, $${totalIncome} in income, $${totalExpenses} in expenses`
    }
}

// Expense --> description, amount

// addExpense --> description, amount
// getAccountSummary --> total up all expenses --> Lisandro Pastinante has $1860 in expenses


account.addExpense('Rent', 950)
account.addExpense('Coffe', 2)

account.addIncome('Job', 1000)

console.log(account.getAccountSummary()) 

// add income array to account
// addIncome method --> description, amount
// tweak getAccountSummary --> Lisandro Pastinante has a balance of $10, $100 in income, $90 in expenses

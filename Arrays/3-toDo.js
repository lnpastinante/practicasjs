// Displace Todos as:
// 1. The frist item
// 2. The second item

const toDo = [' wake up', ' have breakfast', ' study ', ' go to work ', ' do some exercise ']

toDo.splice(2,1)
toDo.push(' go to sleep')
toDo.shift()

console.log(`You have ${toDo.length} todos:`)

toDo.forEach(function(item, index){
    console.log(`${index + 1}. ${item}`)
})
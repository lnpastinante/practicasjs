const notes = ['Note 1', 'Note 2', 'Note 3']

/* console.log(notes.pop())
notes.push('My new note')

console.log(notes.shift())
notes.unshift('My first note') */

//notes.splice(1,1) //Deletes the second item
//notes.splice(1,0,'New note') //adds an item to the list
//notes.splice(1,1,'New second note') //replaces an item in the list

notes[2] = "New Note 3"

console.log(notes.length)
console.log(notes)
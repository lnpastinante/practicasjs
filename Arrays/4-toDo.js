const toDo = [' wake up', ' have breakfast', ' study ', ' go to work ', ' do some exercise ']

toDo.splice(2,1)
toDo.push(' go to sleep')
toDo.shift()

console.log(`You have ${toDo.length} todos:`)

for (i=0; i < toDo.length; i++) {
    console.log(`${i + 1}. ${toDo[i]}`)
}
const toDo = [
    {text: 'Wake up', completed: true},
    {text: 'Have breakfast', completed:false},
    {text: 'Study', completed:true},
    {text: 'Go to work', completed: true},
    {text: 'Do some exercise', completed: false},
]

const sortTodos = function(array) {
    array.sort(function(a,b){
        if (a.completed === true && b.completed === false) {
            return 1
        }
        else if (a.completed === false && b.completed === true) {
            return -1
        }
        else {return 0}
    })
}

sortTodos(toDo)
console.log(toDo)
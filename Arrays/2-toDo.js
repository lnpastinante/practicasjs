// Remove the 3rd item
// Add a new item onto the end
// Remove the first item from the list
// Print all todos on screen

const toDo = [' wake up', ' have breakfast', ' study ', ' go to work ', ' do some exercise ']

toDo.splice(2,1)
toDo.push(' go to sleep')
toDo.shift()

console.log(`You have ${toDo.length} todos`)
console.log(`Todos:${toDo}`)
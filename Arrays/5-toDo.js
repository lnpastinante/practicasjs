// 1. Conver array to array of objects --> text, completed (boolean)
// 2. Create function to remove a to do by text value

const toDo = [
    {text: 'Wake up', completed: true},
    {text: 'Have breakfast', completed:false},
    {text: 'Study', completed:true},
    {text: 'Go to work', completed: true},
    {text: 'Do some exercise', completed: false},
]

const deleteTask = function(array, text) {
    index = array.findIndex(function(item){
        return item.text.toLowerCase() === text.toLowerCase()
        
    })
    array.splice(index,1)
}

deleteTask(toDo, 'study')
console.log(toDo)
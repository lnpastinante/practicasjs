const notes = ['Note 1', 'Note 2', 'Note 3']

console.log(notes)
console.log(notes.length)

console.log(notes[0])
console.log(notes[1])
console.log(notes[2])
console.log(notes[3])

console.log(notes[notes.length-1]) //grab the last element
const notes = [
    {
        title: 'My next trip',
        body: 'I would like to go to Spain'
    },
    {
        title: 'Habits to work on',
        body: 'Exercise, eating a bit better'
    },
    {
        title: 'Office modifications',
        body: 'Get a new seat'
    }
]

// .filter() creates a new array with objects that contain a match for a certain string

const findNotes = function(notes, query) {
    return notes.filter(function(note,index){
        const isTitleMatch = note.title.toLowerCase().includes(query.toLocaleLowerCase())
        const isBodyMatch = note.body.toLocaleLowerCase().includes(query.toLocaleLowerCase())
        return isTitleMatch || isBodyMatch
    })
}



console.log(findNotes(notes, 'work'))
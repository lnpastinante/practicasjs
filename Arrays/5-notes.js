/* 
const notes = ['Note 1', 'Note 2', 'Note 3']

console.log(notes.indexOf('Note 2'))
console.log(notes.indexOf('papitas')) //If there is no such element it returns -1
 */

 const notes = [
     {
         title: 'My next trip',
         body: 'I would like to go to Spain'
     },
     {
         title: 'Habits to work on',
         body: 'Exercise, eating a bit better'
     },
     {
         title: 'Office modifications',
         body: 'Get a new seat'
     }
]


/* 
const findNote = function (notes, noteTitle) {
    const index = notes.findIndex(function (note, index) {
        return note.title.toLowerCase() === noteTitle.toLowerCase()
    })
    return notes[index]
}

const note = findNote(notes, 'office modifications')
  */

  //CAN ALSO BE DONE WITH .find() the difference is that find returns the object itself instead of the index

const findNote = function (notes, noteTitle) {
    return notes.find(function (note, index) {
        return note.title.toLowerCase() === noteTitle.toLowerCase()
    })
}

const note = findNote(notes, 'office modifications')


//Intresting fact
/* 
console.log(notes.indexOf(     
    {
    title: 'My next trip',
    body: 'I would like to go to Spain'
    }
))
  */
 // that code returns -1 beacause an object is only === to himself
// to get it to work we need to use .findIndex()

const index = notes.findIndex(function(note, index) {
    return note.title === 'Habits to work on'
})

//console.log(notes)
//console.log(index)
console.log(note)
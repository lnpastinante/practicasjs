'use strict'

// Primitive value: string, number, boolean, null, undefined

// Object: --> Object.prototype --> null

// Array --> Array.prototype --> Object.prototype --> null

// Function --> Function.prototype --> Object.prototype --> null

// String --> String.prototype --> Object.prototype -->null

// Number --> Number.prototype --> Object.prototype -->null

// Boolean --> Boolean.prototype --> Object.prototype -->null
 
/* 

const product = 'Computer'

console.log(product) //has no properties
console.log(product.split(''))// but when called with a method it gets converted into an object

const otherProduct = new String('Phone') //has properties beacause it was created as a new instance of an object
console.log(otherProduct) // we can see the list of methods in the __proto__
 */

/* 
const team = ['luke', 'maddison']

console.log(team.hasOwnProperty('length')) //hasOwnProperty is an object prototype

const getScore = () => 1

console.log(getScore)
  */

/*


const product = {
    name: 'Influence'
}

// Object.prototype.hasOwnProperty = () => 'This is the new function'
Object.prototype.someNewMethod = () => 'This is the new method'

// hasOwnProperty
console.log(product.hasOwnProperty('hasOwnProperty')) // false (product does not have product.prototype.hasOwnProperty)
console.log(product.someNewMethod()) // false (product does not have product.prototype.hasOwnProperty)

console.log(product)

*/
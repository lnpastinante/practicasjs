/* 
const add = function () {
    return arguments[0] + arguments[1]
} */


const add = (a, b) => arguments[0] + arguments[1] // Arrow functions do not have acces to argeuments


console.log(add(11, 22, 33, 44))

/* 
const car = {
    color: 'Red',
    getSummary: function() {
        return `The car is ${this.color}`
    }
}

console.log(car.getSummary())
  */

 const car = {
    color: 'Red',
    getSummary: () => `The car is ${this.color}` //Arrow functions do not have acces to this
}

console.log(car.getSummary())

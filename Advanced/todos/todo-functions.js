'use strict'

// Load data from localStorage

const loadTodos = (toDo) => {
    const savedTodos = localStorage.getItem('toDo')
    try {
        return savedTodos ? JSON.parse(savedTodos) : []
    } catch (error) {
        return []
    }
}

// Save data to localStorage

const saveTodos = (toDo) => {
    const dataToStore = JSON.stringify(toDo)
    localStorage.setItem('toDo',dataToStore)
}

// Remove todos
const removeTodo = (id) => {
    const removedTodoIndex = toDo.findIndex((item) => item.id === id)
    if (removedTodoIndex > -1) {
        toDo.splice(removedTodoIndex,1)
    }
}

// Things left to do

const thingsLeft = () => {
    const unfinished = toDo.filter((item) => item.completed === false)
    document.querySelector('#things-left').innerHTML = `You have ${unfinished.length} tasks left`
} 

// Render toDos by text filter

const searchText = (array, filter) => {
    const filteredTodos = array.filter((item) => {
        if (hideCompleted === false) {
            return item.text.toLowerCase().includes(filter.text.toLowerCase())
        }
        else {
            return (item.text.toLowerCase().includes(filter.text.toLowerCase()) && item.completed === false)
        }
    })

    document.querySelector('#todos').innerHTML = ''

    filteredTodos.forEach((item) => {
        const thingTodoDiv = document.createElement('div')
        document.querySelector('#todos').appendChild(thingTodoDiv)

        // Create checkbox
        const ThingTodoCheckbox = document.createElement('input')
        ThingTodoCheckbox.setAttribute('type', 'checkbox')
        thingTodoDiv.appendChild(ThingTodoCheckbox)
        if (item.completed === true) {
            ThingTodoCheckbox.setAttribute('checked', true)
        }
        ThingTodoCheckbox.addEventListener('change', (e) => {
            item.completed = e.target.checked
            thingsLeft()
            saveTodos(toDo)
        })

        // Create todo
        const thingTodo = document.createElement('span')
        thingTodo.textContent = item.text
        thingTodoDiv.appendChild(thingTodo)

        //  Create remove button
        const button = document.createElement('button')
        button.textContent = 'x'
        thingTodoDiv.appendChild(button)
        button.addEventListener('click', () => {
            removeTodo(item.id)
            searchText(toDo,search)
            saveTodos(toDo)
        })
        thingsLeft()
    })
}
'use strict'

const toDo =  loadTodos()


const search = {
    text:''
}


let hideCompleted = false


 searchText(toDo,search)


document.querySelector('#filter-task').addEventListener('input', (e) => {
    search.text = e.target.value
    searchText(toDo,search)
})

document.querySelector('#add-form').addEventListener('submit', (e) => {
    e.preventDefault()
    newForm = {id:uuidv4(),text:e.target.elements.task.value, completed: false}
    toDo.push(newForm)
    e.target.elements.task.value = ''
    searchText(toDo,search)
    saveTodos(toDo)
})
 
document.querySelector('#hide-completed').addEventListener('change', (e) => {
    hideCompleted = e.target.checked
    searchText(toDo,search)
})

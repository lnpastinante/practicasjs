/* const myAge = 27

const message = myAge >=18? 'You can vote!' : 'You cannot vote'

console.log(message) */

const myAge = 27
const showPage = () => 'Showing the page'
const showErrorPage = () => 'Showing the error page'

message = myAge >= 21 ? showPage() : showErrorPage()
console.log(message)

// 1. Print "Team size: 3" if less than or equal to 4
// 2. Print "Too many people on your team"
const team = ['Tyler', 'Porter']

addTeammate = (name) => team.push(name)

addTeammate('Jeff')
addTeammate('Ray')
addTeammate('Carl')

team.length > 4 ? console.log('Too many people on your team') : console.log(`Team size: ${teamSize}`) 


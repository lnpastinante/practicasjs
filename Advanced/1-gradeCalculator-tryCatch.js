// students score, total score
// 15/20 -> You got a C (75%)!
// A: 90-100, B: 80-89, C: 70-79, D: 60-69, 0-59 F


let grade = function(score = 0, total= 100) {
    if (typeof score !== 'number' || typeof total !== 'number') {
        throw Error('The score and the total must be numbers')
    }
    perc = score/total
    if (perc >= .9) {
        return `You got an A ${perc}%!`
    }
    else if (perc >=.8) {
        return `You got a B ${perc}%!`
    }
    else if (perc >=.7) {
        return `You got a C ${perc}%!`
    }
    else if (perc >=.6) {
        return `You got a D ${perc}%!`
    }
    else {
        return `You got an F ${perc}%!`
    }
}

try {
    console.log(grade('caca',20))
}
catch (e) {
    console.log(e.message)
}
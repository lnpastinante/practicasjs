let name = '  Lisandro Pastinante  '

// Length property
console.log(name.length)

// Convert to upper case

console.log(name.toUpperCase())

// Conver to lower case
console.log(name.toLowerCase())

// Includes method
let password = 'abc123password098'
console.log(password.includes('password'))

// Trim
name = name.trim()
console.log(name.length)
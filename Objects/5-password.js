// isValidPassword
// Length is more than 8 and it doesn't contain the word password

let isValidPassword = function(pass) {
    if (pass.length < 8 || pass.includes('password')) {
        return false
    }
    else{
        return true
    }
}

console.log(isValidPassword('asdf'))
console.log(isValidPassword('abc123!@#$%^&'))
console.log(isValidPassword('asdkjlaslkpassword'))
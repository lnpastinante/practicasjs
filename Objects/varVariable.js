var firstName = 'Andrew'
firstName = 'Mike'

var firstName = 'Jen'

console.log(firstName)

if (10 === 10) {
    var age = 27
}

console.log(age)

// Var variables are function scoped

const setName = function () {
    var firstName = 'Jen'
}

setName()

//console.log(firstName)

//Var defined variables don't break the code if you call them before declaring them
//Var declarations get hoisted to the top of the code

console.log(papas)
var papas
//let papas
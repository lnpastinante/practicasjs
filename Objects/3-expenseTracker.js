// addIncome(account.amount)
// addExpense(account.amount)
//resetAccount(account)

//getAccountSummary
// Account for Andrew has $900, $100 in income, $100 in expenses

//addIncome
//addExpense
//addExpense
//getAccountSummary
//resetAccount
//getAccountSummary

let myAccount = {
    name: 'Andrew',
    money:900,
    expenses: 100,
    income: 100
}

let addIncome = function (account,amount) {
    account.income = account.income + amount
}

let addExpense = function(account,amount) {
    account.expenses = account.expenses + amount
}

let resetAccount = function(account) {
    account.money = 0
    account.expenses = 0
    account.income = 0
}

let getAccountSummary = function(account) {
    console.log(`Account for ${account.name} has ${account.money}$, ${account.income}$ income, ${account.expenses}$ in expenses.`)
}

addIncome(myAccount,20)
addExpense(myAccount,10)
addExpense(myAccount,5)
getAccountSummary(myAccount)
resetAccount(myAccount)
getAccountSummary(myAccount)
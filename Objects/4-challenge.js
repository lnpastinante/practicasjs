let restaurant = {
    name: 'De tal Palo',
    capacity: 75,
    guests: 0,
    checkAvailability: function(partySize) {
        let seatsLeft = this.capacity - this.guests
        return partySize <=seatsLeft
    },
    seatParty: function (partySize) {
        this.guests = this.guests + partySize
    },
    removeParty: function (partySize) {
        this.guests = this.guests - partySize
    }
}
// seatParty
// removeParty

restaurant.seatParty(72)
console.log(restaurant.checkAvailability(4))
restaurant.removeParty(5)
console.log(restaurant.checkAvailability(4))
let farenheit = 50

let celcius = (farenheit - 32)*5/9
let kelvin = celcius + 273.15

let convert = function(F){
    return {
        farenheit: F,
        celcius: (F - 32)*5/9,
        kelvin: (F - 32)*5/9 + 273.15
    }
}

console.log(convert(50))
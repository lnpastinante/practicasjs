let num = 183.941

console.log(num.toFixed(2))

console.log(Math.round(num))
console.log(Math.floor(num))
console.log(Math.ceil(num))

let min = 10
let max = 20
let randomnum = Math.floor(Math.random()*(max - min + 1)) + min
// Math-random() generates numbers in [0;1)
console.log(randomnum)
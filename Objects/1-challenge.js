// name, age, location

// Andrew is 27 and lives in Philadelphia
//Increase age by 1 and print message again

let person = {
    name: 'Andrew',
    age: 27,
    location: 'Philadelphia'
}

console.log(`${person.name} is ${person.age} and lives in ${person.location}`)

person.age = person.age + 1

console.log(`${person.name} is ${person.age} and lives in ${person.location}`)